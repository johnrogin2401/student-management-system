import express from "express";
import { studentRegister } from "../controller/student_route_function";

const studentRoute = express();

studentRoute.post('/register', studentRegister)

export default studentRoute
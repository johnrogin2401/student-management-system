import express from "express";
import {adminDashboard, studentRegister ,adminRegister, adminLogin, validateToken} from "../controller/admin_route_function.js";

const adminRoute = express();

adminRoute.post('/login',adminLogin ,validateToken);
adminRoute.post('/register', adminRegister);
adminRoute.get('/dashboard', adminDashboard)

export default adminRoute
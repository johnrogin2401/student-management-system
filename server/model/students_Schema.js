import mongoose from "mongoose";
// import bcrypt from "bcryptjs";

const studentSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    course: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    address: {
        type: String
    },
    pinCode: {
        type: String
    },
    phoneNumber: {
        type: String
    }
});

const Students = mongoose.model("Students", studentSchema);

export { Students }
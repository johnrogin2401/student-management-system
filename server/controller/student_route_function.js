// import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

//  middleware for students registration
async function studentRegister(req, res) {                          
    try {                                        
        const newStudent = new Students(req.body);
        newStudent.password = await bcrypt.hash(String(newStudent.password), 10)
        const savedStudent = await newStudent.save();
        res.status(200).json(savedStudent);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
}

export {studentRegister}
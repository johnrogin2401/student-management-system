import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();
import adminRoute from "./routes/admin_route.js";
import studentRoute from "./routes/student_route.js";

const app = express();
app.use(express.json())

mongoose.connect(process.env.URI)
   .then(() => console.log('Connected to DataBase'))
   .catch((err) => console.log(err));

app.use('/admin', adminRoute)
app.use('/student', studentRoute)

app.listen(process.env.PORT, () =>{
    console.log(`Server is running at ${process.env.PORT}`);
})
